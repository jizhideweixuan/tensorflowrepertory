# -*- coding: utf-8 -*-
"""
Created on Fri Aug 18 09:00:51 2017

@author: Administrator
"""

import matplotlib.pyplot as plt
import tensorflow as tf
import numpy as np

image_raw_data = tf.gfile.FastGFile("./pic/cat.jpg", 'rb').read()


def PrintImg(con_data, description):
    print("描述: ", description)
    print("Digital type: ", con_data.dtype)
    plt.imshow(np.asarray(con_data.eval(), dtype='uint8'))
    plt.show()

with tf.Session() as sess:
    # 对图片进行解码
    img_data = tf.image.decode_jpeg(image_raw_data)
    # 输出图片三维矩阵
    print(img_data.eval())
    # 使用pypylot获得可视化图像
    plt.imshow(img_data.eval())
    # 显示图像
    plt.show()
    """
    # 缩放
    # 通过tf.image.resize_images重新调整图像的大小
    resized = tf.image.resize_images(img_data, [300, 300], method=0)
    PrintImg(resized, "缩放")

    # 剪裁,填充
    # 使用tf.image.resize_image_with_crop_or_pad自动裁剪,填充
    croped = tf.image.resize_image_with_crop_or_pad(img_data, 1000, 1000)
    PrintImg(croped, "裁剪")
    padded = tf.image.resize_image_with_crop_or_pad(img_data, 3000, 3000)
    PrintImg(padded, "填充")
    # 通过比例调整图像大小
    central_cropped = tf.image.central_crop(img_data, 0.5)
    PrintImg(central_cropped, "通过比例裁剪")

    # 翻转
    # 上下翻转
    flipped = tf.image.flip_up_down(img_data)
    PrintImg(flipped, "上下翻转")
    # 左右翻转
    flipped = tf.image.flip_left_right(img_data)
    PrintImg(flipped, "左右翻转")
    # 将图像沿对角线翻转
    transposed = tf.image.transpose_image(img_data)
    PrintImg(transposed, "将图像沿对角线翻转")
    # 以一定概率上下翻转图像
    flipped = tf.image.random_flip_up_down(img_data)
    PrintImg(flipped, "以一定概率上下翻转图像")
    # 以一定的概率左右
    flipped = tf.image.random_flip_left_right(img_data)
    PrintImg(flipped, "以一定概率左右翻转图像")

    # 将图像的亮度-0.5
    adjusted = tf.image.adjust_brightness(img_data, -0.5)
    PrintImg(adjusted, "将图像的亮度-0.5")
    # 将图像的亮度+0.5
    adjusted = tf.image.adjust_brightness(img_data, 0.5)
    PrintImg(adjusted, "将图像的亮度+0.5")
    # 将图像的亮度在[-max_delta,max_delta]中随机调节
    adjusted = tf.image.random_brightness(img_data, 1.0)
    PrintImg(adjusted, "将图像的亮度在[-max_delta,max_delta]中随机调节")

    # 将图像的对比度-5
    adjusted = tf.image.adjust_contrast(img_data, -5)
    PrintImg(adjusted, "将图像的对比度-5")
    # 将图像的对比度+5
    adjusted = tf.image.adjust_contrast(img_data, +5)
    PrintImg(adjusted, "将图像的对比度+5")
    # 在[lower,upper]的范围内随机调整
    adjusted = tf.image.random_contrast(img_data, 0, 5)
    PrintImg(adjusted, "在[lower,upper]的范围内随机调整")

    # 色相+0.1
    adjusted = tf.image.adjust_hue(img_data,0.1)
    PrintImg(adjusted, "色相+0.1")
    # 色相+0.3
    adjusted = tf.image.adjust_hue(img_data, 0.3)
    PrintImg(adjusted, "色相+0.3")
    # 色相+0.6
    adjusted = tf.image.adjust_hue(img_data, 0.6)
    PrintImg(adjusted, "色相+0.6")
    # 色相+0.9
    adjusted = tf.image.adjust_hue(img_data, 0.9)
    PrintImg(adjusted, "色相+0.9")
    # 在[-max_delta,max_delta]之间，max_delta在[0,0.5]之间
    adjusted = tf.image.random_hue(img_data,0.5)
    PrintImg(adjusted, "在[-max_delta,max_delta]之间，max_delta在[0,0.5]之间")

    # 将图像的饱和度-5
    adjusted = tf.image.adjust_saturation(img_data, -5)
    PrintImg(adjusted, "将图像的饱和度-5")
    # 将图像的饱和度+5
    adjusted = tf.image.adjust_saturation(img_data ,5)
    PrintImg(adjusted, "将图像的饱和度+5")
    # [lower,upper]的范围随机调整图的饱和度
    adjusted = tf.image.random_saturation(img_data, 0, 5)
    PrintImg(adjusted, "[lower,upper]的范围随机调整图的饱和度")
    """

    # 将图像缩小一些，这样可视化能让标注框更加清楚
    img_data = tf.image.resize_images(img_data,[180,267],method=1)

    # 给出每一张图像的所有标注框，一个标注框有四个数字，分别代表[Ymin,Xmin,Ymax,Xmax]
    # 注意这里给出的数字都是图像的相对位置，比如在180*267的图像中，[0.35,0.47,0.5,0.56]代表到（63，125）到（90，150）
    boxes = tf.constant([[[0.05, 0.05, 0.9, 0.7], [0.35, 0.47, 0.5, 0.56]]])
    # 通过标注随机截取，告诉截取函数，哪些是有信息量的
    begin, size, bbox_for_draw = tf.image.sample_distorted_bounding_box(
        tf.shape(img_data), bounding_boxes=boxes,min_object_covered=0.1)
    # 将图像矩阵转化为实数类型，tf.image.draw_bounding_boxes()是输入是一个batch数据，也就是多张图组成的思维矩阵
    # 所以要将解码后的图像矩阵加1维
    batched = tf.expand_dims(tf.image.convert_image_dtype(img_data, tf.float32), 0)
    image_with_box = tf.image.draw_bounding_boxes(batched,boxes)
    plt.imshow(image_with_box[0].eval())
    plt.show()
    #根据box随机截取
    distorted_image = tf.slice(img_data,begin,size)
    PrintImg(distorted_image,"distorted_image")

    img_data = tf.image.convert_image_dtype(img_data, dtype=tf.uint8)
    # 将一张图像的三维矩阵重新按照jpeg编码并存入文件中
    encode_image = tf.image.encode_jpeg(img_data)
    with tf.gfile.GFile("./out/output.jpeg", 'wb') as f:
        f.write(encode_image.eval())
