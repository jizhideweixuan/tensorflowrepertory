# -*- coding: utf-8 -*-

import os

import tensorflow as tf
from tensorflow.examples.tutorials.mnist import input_data

# 加载mnist_infernece.py中的定义常量和向前传播函数
import mnist_infernece

# 配置神经网络的参数
# 数据集
BATCH_SIZE = 100
# 基础学习率
LEARNING_RATE_BASE = 0.8
# 学习率衰减
LEARNING_RATE_DECAY = 0.99
# 正则化权重
REGULARAZTION_RATE = 0.0001
# 训练次数
TRAINING_STEPS = 30000
# 滑动平均衰减率
MOVING_AVERAGE_DECAY = 0.99
# 模型保存的路径和文件名
MODEL_SAVE_PATH = "./model/"
MODEL_NAME = "model.ckpt"


# 定义训练函数
def train(mnist):
    # 定义输出placeholder
    x = tf.placeholder(tf.float32, [None, mnist_infernece.INPUT_NODE], name='x-input')
    y_ = tf.placeholder(tf.float32, [None, mnist_infernece.OUTPUT_NODE], name='y-input')

    # l2正则化
    regularizer = tf.contrib.layers.l2_regularizer(REGULARAZTION_RATE)
    # 模型输出
    y = mnist_infernece.infernece(x, regularizer)
    # step模拟迭代次数
    global_step = tf.Variable(0, trainable=False)

    # 定义一个滑动平均的类,定义时给出衰减率,step
    variable_averages = tf.train.ExponentialMovingAverage(MOVING_AVERAGE_DECAY, global_step)
    # 定义一个滑动平均操作,给定一个列表,每次执行这个操作时,列表值刷新
    variables_averages_op = variable_averages.apply(tf.trainable_variables())

    # 定义交叉熵
    cross_entropy = tf.nn.sparse_softmax_cross_entropy_with_logits(logits=y, labels=tf.argmax(y_, 1))
    # 平均交叉熵
    cross_entropy_mean = tf.reduce_mean(cross_entropy)
    # 定义损失函数
    loss = cross_entropy_mean + tf.add_n(tf.get_collection('losses'))
    # 定义学习率,参数:基础学习率,step,数据集数量,衰减率
    learning_rate = tf.train.exponential_decay(LEARNING_RATE_BASE, global_step,
                                               mnist.train.num_examples / BATCH_SIZE,
                                               LEARNING_RATE_DECAY,
                                               staircase=True)
    # 定义训练步骤,GradientDescentOptimizer为梯度下降优化器,用于优化损失率
    train_step = tf.train.GradientDescentOptimizer(learning_rate).minimize(loss, global_step=global_step)
    with tf.control_dependencies([train_step, variables_averages_op]):
        train_op = tf.no_op(name='train')

    saver = tf.train.Saver()
    with tf.Session() as sess:
        tf.global_variables_initializer().run()

        for i in range(TRAINING_STEPS):
            xs, ys = mnist.train.next_batch(BATCH_SIZE)
            _, loss_value, step = sess.run([train_op, loss
                                               , global_step], feed_dict={x: xs, y_: ys})

            if i % 1000 == 0:
                print("After %d training step(s),loss on training batch is %g." % (step, loss_value))
                saver.save(sess, os.path.join(MODEL_SAVE_PATH, MODEL_NAME), global_step=global_step)


def main(argv=None):
    mnist = input_data.read_data_sets("./tmp/data", one_hot=True)
    train(mnist)


if __name__ == '__main__':
    tf.app.run()
