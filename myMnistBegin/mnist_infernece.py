# -*- coding: utf-8 -*-

# 导入包
import tensorflow as tf

# 定义神经结构相关参数
# 输入节点
INPUT_NODE = 784
# 输出节点
OUTPUT_NODE = 10
# 第一层节点
LAYER1_NODE = 500


# tf.get_veriable函数获取变量
# 定义获取变量函数
def get_weight_variable(shape, regularizer):
    weights = tf.get_variable(
        "weights", shape,
        initializer=tf.truncated_normal_initializer(stddev=0.1))
    # 损失集合losses
    # 判断是否存在正则化
    if regularizer != None:
        # 使用tf.add_to_collection将一个张量加入losses集合
        tf.add_to_collection('losses', regularizer(weights))
    return weights


# 定义神经网络向前传播
def infernece(input_tensor, regularizer):
    # 声明第一层变量并且完成传播过程
    with tf.variable_scope('layer1'):
        # 权重变量
        weights = get_weight_variable([INPUT_NODE, LAYER1_NODE], regularizer)
        # 激活变量
        biases = tf.get_variable(
            "biases", [LAYER1_NODE], initializer=tf.constant_initializer(0.0))
        # 第一层输出
        layer1 = tf.nn.relu(tf.matmul(input_tensor, weights) + biases)

    # 声明第二层视神经网络变量
    with tf.variable_scope('layer2'):
        # 权重
        weights = get_weight_variable([LAYER1_NODE, OUTPUT_NODE], regularizer)
        # 激活
        biases = tf.get_variable(
            "biases", [OUTPUT_NODE], initializer=tf.constant_initializer(0.0))
        # 第二层输出
        layer2 = tf.matmul(layer1, weights) + biases

    return layer2
