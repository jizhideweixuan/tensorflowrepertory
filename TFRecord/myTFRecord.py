# -*- coding: utf-8 -*-
"""
Created on Thu Aug 17 13:31:25 2017

@author: Administrator
"""
"""
message Exampe {
    Features features = 1;
};
message Feature {
    map<string,　Feature> feature = 1;
};
message Feature {
    oneof kind {
        BytesList bytes_list = 1;
        FloatList float_list = 2;
        int64List int64_list = 3;
    }
};
"""
import tensorflow as tf
from tensorflow.examples.tutorials.mnist import input_data
import numpy as np


# 生成整数型的属性
def _int64_feature(value):
    return tf.train.Feature(int64_list=tf.train.Int64List(value=[value]))


# 圣城字符串属性
def _bytes_feature(value):
    return tf.train.Feature(bytes_list=tf.train.BytesList(value=[value]))


# 8位无符号整形,onehot编码(一位有效编码)
mnist = input_data.read_data_sets("./data", dtype=tf.uint8, one_hot=True)

images = mnist.train.images
# 训练数据所对应的正确答案,可以作为一个属性保存在TFRcord中
labels = mnist.train.labels
# 训练数据的图像分辨率,这可以作为一个属性保存在TFRecord中
pixels = images.shape[1]
# 数据个数
num_examples = mnist.train.num_examples

# 输出TFRecord的文件地址
filename = "./output/output.tfrecords"
# 创建一个writer写TFRecord文件
writer = tf.python_io.TFRecordWriter(filename)
for index in range(num_examples):
    # 将图像矩阵转化为一个字符串
    image_raw = images[index].tostring()
    # 将一个样例转化为Example Protocol Buffer 并将所有信息写入这个数据结构
    example = tf.train.Example(features=tf.train.Features(feature={
        'pixels': _int64_feature(pixels),
        'label': _int64_feature(np.argmax(labels[index])),
        'image_raw': _bytes_feature(image_raw)
    }))
    # 将一个Example写入TFRecord文件
    writer.write(example.SerializeToString())
writer.close()

