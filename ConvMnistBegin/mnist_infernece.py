# -*- coding: utf-8 -*-
"""
Created on Tue Aug 15 16:26:04 2017

@author: Administrator
"""

"""
LeNet-5模型
1.卷积层 输入层28*28*1 过滤器5*5*32 步长1 全0填充 输出28*28*32
2.池化层 输入层28*28*32 过滤器边长为2 步长2 最大池化层 输出为14*14*32
3.卷积层 输入层14*14*32 过滤器 5*5*64 步长1 全0填充 输出为14*14*64
4.池化层 输入层14*14*64 过滤器边长为2 步长为2 最大池化层 输出为7*7*64 
5.计算将矩阵拉成向量后的长度,长*宽*高 第4步输出 → 拉成向量 输出长度为3136的向量 输出为512
6.输出长度为512的向量,输出长度为10的向量,通过softmax得到最后结果
"""
import tensorflow as tf

# 配置神经网络的参数
# 输入节点数
INPUT_NODE = 784
# 输出节点数
OUTPUT_NODE = 10

# 图像尺寸 长=宽
IMAGE_SIZE = 28
# 图像深度
NUM_CHANNELS = 1
# 第六层输出
NUM_LABELS = 10

# 第一层卷积层的尺寸和深度
CONV1_DEEP = 32
CONV1_SIZE = 5
# 第二层卷积层尺寸和深度
CONV2_DEEP = 64
CONV2_SIZE = 5
# 全连接层节点个数
FC_SIZE = 512


# 添加新参数train,用于区分训练与测试,使用dropout进一步提升模型可靠性防止过拟合
# dropout只有在训练时使用
def infernece(input_tensor, train, regularizer):
    # 卷积层1变量范围
    with tf.variable_scope('layer1-conv1'):
        # 权重变量
        conv1_weights = tf.get_variable("weight", [CONV1_SIZE, CONV1_SIZE, NUM_CHANNELS, CONV1_DEEP],
                                        initializer=tf.truncated_normal_initializer(stddev=0.1))
        # 激活变量
        conv1_biases = tf.get_variable("bias", [CONV1_DEEP], initializer=tf.constant_initializer(0.0))
        # 使用边长为5,深度为32的过滤器,过滤移动的步长为1,且使用全0填充
        conv1 = tf.nn.conv2d(input_tensor, conv1_weights, strides=[1, 1, 1, 1], padding='SAME')
        relu1 = tf.nn.relu(tf.nn.bias_add(conv1, conv1_biases))

    # 池化层1变量范围
    with tf.name_scope('layer2-pool1'):
        pool1 = tf.nn.max_pool(relu1, ksize=[1, 2, 2, 1], strides=[1, 2, 2, 1], padding='SAME')

    # 卷积层2
    with tf.variable_scope('layer3-conv2'):
        # 权重变量
        conv2_weights = tf.get_variable("weight", [CONV2_SIZE, CONV2_SIZE, CONV1_DEEP, CONV2_DEEP],
                                        initializer=tf.truncated_normal_initializer(stddev=0.1))
        # 激活变量
        conv2_biases = tf.get_variable("bias", [CONV2_DEEP], initializer=tf.constant_initializer(0.0))
        # 使用边长为5,深度为64的过滤器,过滤器步长为1,且使用全0填充
        conv2 = tf.nn.conv2d(pool1, conv2_weights, strides=[1, 1, 1, 1], padding='SAME')
        relu2 = tf.nn.relu(tf.nn.bias_add(conv2, conv2_biases))
    # 池化层2
    with tf.name_scope('layer4-pool2'):
        pool2 = tf.nn.max_pool(relu2, ksize=[1, 2, 2, 1], strides=[1, 2, 2, 1], padding='SAME')

    # 获得池化层2输出的长宽高
    pool_shape = pool2.get_shape().as_list()
    # 计算将矩阵拉成向量后的长度
    nodes = pool_shape[1] * pool_shape[2] * pool_shape[3]
    # 通过tf.reshape函数将第四层输出变成一个batch向量
    reshaped = tf.reshape(pool2, [pool_shape[0], nodes])

    with tf.variable_scope('layer5-fc1'):
        fc1_weights = tf.get_variable("weight", [nodes, FC_SIZE],
                                      initializer=tf.truncated_normal_initializer(stddev=0.1))
        if regularizer != None:
            tf.add_to_collection('losses', regularizer(fc1_weights))
        fc1_biases = tf.get_variable("bias", [FC_SIZE], initializer=tf.constant_initializer(0.1))
        fc1 = tf.nn.relu(tf.matmul(reshaped, fc1_weights) + fc1_biases)
        if train:
            fc1 = tf.nn.dropout(fc1, 0.5)

    with tf.variable_scope('layer6-fc2'):
        fc2_weights = tf.get_variable("weight", [FC_SIZE, NUM_LABELS],
                                      initializer=tf.truncated_normal_initializer(stddev=0.1))
        if regularizer != None:
            tf.add_to_collection('losses', regularizer(fc2_weights))
        fc2_biases = tf.get_variable("bias", [NUM_LABELS], initializer=tf.constant_initializer(0.1))
        logit = tf.matmul(fc1, fc2_weights) + fc2_biases

    return logit
