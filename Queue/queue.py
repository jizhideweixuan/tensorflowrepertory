# -*- coding: utf-8 -*-
"""
Spyder Editor

This is a temporary script file.
"""

"""
TensorFlow提供FIFOQueue（先进先出）和RandomShuffleQueue（随机）两种队列
"""
import tensorflow as tf

#创建一个先进先出队列，指定队列中最多可以保存两个元素
q = tf.FIFOQueue(2,"int32")
#使用enqueue_many函数来初始化队列中的元素，和变量初始化类型，在使用队列之前需要明确的调用这个初始化过程
init = q.enqueue_many(([0,10],))
#使用Dequeue函数将队列中的第一个元素出队，存放在变量X中
x = q.dequeue()
#将得到的值+1
y = x + 1
#将+1后的值在重新加入队列
q_inc= q.enqueue([y])

with tf.Session() as sess:
    #运行初始化队列的操作
    init.run()
    for _ in range(5):
        #运行q_inc将数据出队，出队的元素+1，再重新加入队列
        v, _ = sess.run([x, q_inc])
        print(v)