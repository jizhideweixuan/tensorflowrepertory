# -*- coding: utf-8 -*-
"""
Created on Mon Aug 21 12:09:59 2017

@author: t4679
"""

import tensorflow as tf
#声明一个先进先出的队列，队列中最多100个元素，类型为实数
queue = tf.FIFOQueue(100,"float")
#定义队列入队操作
enqueue_op = queue.enqueue([tf.random_normal([1])])

#使用tf.train.QueueRunner来创建多个线程运行队列的入队操作
#第一个参数给出要操作的队列，
#第二个参数[enqueue_op]*5表示需要启动5个线程，每个线程运行enqueue_op操作
qr = tf.train.QueueRunner(queue,[enqueue_op]*5)

#将定义过的QueueRunner加入TensorFlow计算图上指定的集合
#tf.train.QueueRunner没有指定的集合，则加入默认集合tf.GraphKeys.QUEUE_RUNNERS
tf.train.add_queue_runner(qr)
#定义出队操作
out_tensor = queue.dequeue()

with tf.Session() as sess:
    #使用tf.Coordinator来协同启动的线程
    coord = tf.train.Coordinator()
    #需要明确调用tf.train.start_queue_runners启动所有线程，
    #否则当调用出队操作时，程序会一直等待入队操作，
    #tf.train.start_queue_runners会默认启动tf.GraphKeys.QUEUE_RUNNERS中的所有runner
    threads = tf.train.start_queue_runners(sess=sess,coord=coord)
    #获取队列中的值
    for _ in range(3) : print( sess.run(out_tensor)[0])
    #使用tf.train.Coordinator来停止所有线程
    coord.request_stop()
    coord.join(threads)    