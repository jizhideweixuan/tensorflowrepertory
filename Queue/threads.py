# -*- coding: utf-8 -*-
"""
Created on Sun Aug 20 17:56:26 2017

@author: t4679
"""

"""
线程协助类，tf.Coordinator,tf.QueueRunner
tf.Coordinator提供should_stop,request_stop,join
声明tf.Coordinator并传入每一个创建的线程中
启动的线程需要一直查询should_stop，当这个函数返回True时当前线程退出
每一个启动的线程都可以通过request_stop通知其他线程退出，
当某线程调用request_stop后，should_stop的返回值将设置为True
"""

import tensorflow as tf 
import numpy as np
import threading
import time 
#线程中运行的程序，这个程序每隔1秒判断是否需要停止并且打印自己的ID
def MyLoop (coord, worker_id):
    #使用tf.CoorDinator类提供的协同工具判断当前线程是否需要停止
    while not coord.should_stop():
        #随机停止所有的线程
        if np.random.rand() < 0.1:
            print ("Stoping from id: %d\n"%(worker_id))
            coord.request_stop()
        else:
            #打印当前的线程ID
            print ("Working on id: %d\n"%(worker_id))
        #暂停1秒
        time.sleep(1)

#声明一个tf.train。Coordinator类来协同多个线程
coord = tf.train.Coordinator()
#声明创建5个线程
threads = [threading.Thread(target=MyLoop, args=(coord, i, )) for i in range(5)]
for t in threads:t.start()
#等待所有线程退出
coord.join(threads)