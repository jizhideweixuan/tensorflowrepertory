"""
tf.train.match_filenames_once() 获取符合一个正则表达式的所有文件
tf.train.string_input_producer() 管理得到的文件列表
tf.train.string_input_producer()：使用初始化时的文件列表创建一个输入队列，创建好后输入队列可以作为文件读取函数的参数。
每次调用文件读取函数时，该函数会先判断当前是否又打开的文件可读，
如果没有或者打开的文件已经读完，这个函数会从输入队列中出队一个文件并且从这个文件中读取
shuffle参数，使tf.train.string_input_producer()支持随机打乱列表文件的入队和出队，
num_epochs参数，限制加载初始文件列表的最大轮数，若达到轮数后尝试读取，会报OutOfRange错误

再不打乱文件列表的情况下，会一次读出样例中的每一个样例，而且当所有样例都被读取完后，程序会自动从头开始，
若限制num_epochs为1，程序会报错

通过tf.train.batch和tf.train.shuffle_batch函数来讲单个的样例组织成batch的形式输出
这两个函数都会生成一个队列，队列的入队操作时生成单个样例的方法，入队操作时生成单个样例的方法
每次出队得到一个batch的样例，他们唯一的区别在于是否会打乱数据顺序
"""

import tensorflow as tf
#使用tf.train.match_filenames_onece函数来获取文件列表
files = tf.train.match_filenames_once("./data/data.tfrecords-*")
#通过tf.train.string_input_producer创建输入队列，列表为tf.train.match_filenames_once获取的文件列表
#shuffle=Flase不打乱文件读取顺序
filename_queue = tf.train.string_input_producer(files, shuffle=False)
reader = tf.TFRecordReader()
_, serialized_example = reader.read(filename_queue)
features = tf.parse_single_example(
      serialized_example,
      features={
          'i': tf.FixedLenFeature([], tf.int64),
          'j': tf.FixedLenFeature([], tf.int64),
      })
#使用ExampleCreate.py读取并解析的样例，假设Example结构中i表示一个样例的特征向量，比如一张图的像素矩阵，j表示该样例对应标签
example, label = features['i'], features['j']
#样例个数
batch_size = 3
#组合样例的队列中，最多可以存储的样例个数，
#队列太多，则需要占用很多内存资源，太小则可能因为没有数据而被阻碍（block），导致效率降低
capacity = 1000 + 3 * batch_size
example_batch, label_batch = tf.train.batch(
    [example, label],batch_size=batch_size,capacity=capacity)

with tf.Session() as sess:
    #tf.train.match_filenames_once，再使用时需要初始化一些变量
    init = (tf.global_variables_initializer(), tf.local_variables_initializer())
    sess.run(init)
    print("输入文件:")
    print(sess.run(files))
    coord = tf.train.Coordinator()
    threads = tf.train.start_queue_runners(sess=sess, coord=coord)
    print("输出的样例:")
    for i in range(6):
        print(sess.run([features['i'], features['j']]))
    print("组合输出的样例:")
    for i in range(2):
        cur_example_batch,cur_label_batch = sess.run([example_batch,label_batch])
        print( cur_example_batch,cur_label_batch )
    coord.request_stop()
    coord.join(threads)