"""
生成样例数据
以下代码将生成
data.tfrecords-00000-of-00002
data.tfrecords-00001-of-00002
每一个文件中存储了两个样例
"""
import tensorflow as tf

#TFRecord文件的帮助函数
def _int64_feature(value):
    return tf.train.Feature(int64_list=tf.train.Int64List(value=[value]))
#模拟大数据情况下将数据写入不同的文件，num_shards定义了总共写入了多少个文件
num_shards = 2
#instance_per_shard定义了每个文件中又多少个数据
instance_per_shard = 2
for i in range(num_shards):
    # 将数据分为多个文件时，将不同文件以类似0000n-of-0000m后缀区分，m表示数据共被存再几个分页中，n表示当前文件编号
    filename = ('./data/data.tfrecords-%.5d-of-%.5d'%(i,num_shards))
    writer = tf.python_io.TFRecordWriter(filename)
    for j in range(instance_per_shard):
        # Example结构仅包含当前样例属于第几个文件以及时当前文件的第几个样本
        example = tf.train.Example(features=tf.train.Features(feature={
            'i':_int64_feature(i),
            'j':_int64_feature(j)
        }))
        writer.write(example.SerializeToString())
    writer.close()